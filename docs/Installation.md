# Install Independent Push Notifications Service

This guide will explain you how to install this service on your server.

1. Create a dedicated user & switch to it:
```bash
sudo adduser --disabled-login pushservice
sudo -u pushservice bash
```

2. Install Rust
```bash
# Download & install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

3. Clone the repository & enter it
```bash
git clone https://gitlab.com/pierre42100/push-notifications-service
cd push-notifications-service
```

4. Build the project
```shell
cargo build --release
```

5. Initialize configuration & storage directory
```shell
target/release/push-notification-service config.private.yaml init-config
mkdir storage
```

6. Adapt the values inside `config.private.yaml` to make them fit your needs

7. Check the server is working
```shell
target/release/push-notification-service config.private.yaml serve
```

8. Go back to an admin session


9. Create Apache configuration, save it & restart Apache:
```apache2
<VirtualHost *:443>
	DocumentRoot /srv/web/defaultHome

    ServerName pushnotifications.communiquons.org

    ServerAdmin webmaster@localhost

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    RewriteEngine On
    RewriteCond %{REQUEST_URI} "(.*)/ws/(.*)"    [NC]
    RewriteRule "/ws/(.*)" "ws://localhost:4500/ws/$1" [P,L]

    SSLEngine on
    SSLCertificateFile    /etc/letsencrypt/live/communiquons.org-0001/fullchain.pem
    SSLCertificateKeyFile  /etc/letsencrypt/live/communiquons.org-0001/privkey.pem
</VirtualHost>
```

12. Create systemd configuration (in `/etc/systemd/system/pushnotifications.service`):
```shell
[Unit]
Description=Independent Push Notifications Service
After=syslog.target
After=network.target

[Service]
RestartSec=2s
Type=simple
User=pushservice
Group=pushservice
WorkingDirectory=/home/pushservice
ExecStart=/home/pushservice/push-notifications-service/target/release/push-notification-service /home/pushservice/push-notifications-service/config.private.yaml serve
Restart=always
Environment=USER=pushservice 
HOME=/home/pushservice

[Install]
WantedBy=multi-user.target
```

13. Enable new service:
```shell
sudo systemctl enable pushnotifications.service
sudo systemctl start pushnotifications.service
```