//! # Project structures
//!
//! @author Pierre Hubert

pub mod client_token;
pub mod http_error;
pub mod notification;
