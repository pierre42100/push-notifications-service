//! # Notification data
//!
//! Contains all the information related to a notification

use serde::{Deserialize, Serialize};

pub type NotifID = String;

#[derive(Serialize, Deserialize, Clone)]
pub struct Notification {
    pub id: NotifID,
    pub time_create: u64,
    pub timeout: Option<u64>,
    pub title: Option<String>,
    pub body: Option<String>,
    pub image: Option<String>,
}

pub type NotifList = Vec<Notification>;
