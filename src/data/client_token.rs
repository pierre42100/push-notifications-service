//! # Single client token
//!
//! @author Pierre Hubert

use std::hash::Hash;

use serde::{Deserializer, Serializer};

#[derive(Debug, Clone, Ord, PartialOrd, PartialEq, Hash)]
pub struct ClientToken(String);

impl ClientToken {
    pub fn new(token: &str) -> Self {
        ClientToken(token.to_string())
    }

    pub fn id(&self) -> &str {
        &self.0
    }
}

impl serde::Serialize for ClientToken {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        self.0.serialize(serializer)
    }
}

impl<'de> serde::Deserialize<'de> for ClientToken {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        String::deserialize(deserializer).map(ClientToken)
    }
}

impl Eq for ClientToken {}
