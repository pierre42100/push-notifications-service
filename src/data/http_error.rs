//! # Http error
//!
//! @author Pierre Hubert

use std::fmt;
use std::fmt::{Display, Formatter};

use actix_web::http::StatusCode;
use actix_web::ResponseError;

use crate::utils::error::{ExecError, Res};
use std::error::Error;

#[derive(Debug)]
pub struct HttpError {
    status: StatusCode,
    error: String,
}

impl HttpError {
    pub fn new(status: u16, error: &str) -> Res<Self> {
        Ok(Self {
            status: StatusCode::from_u16(status)?,
            error: error.to_string(),
        })
    }

    pub fn unauthorized(error: &str) -> Self {
        Self {
            status: StatusCode::UNAUTHORIZED,
            error: error.to_string(),
        }
    }

    pub fn server_error(error: &str) -> Self {
        Self {
            status: StatusCode::INTERNAL_SERVER_ERROR,
            error: error.to_string(),
        }
    }

    pub fn bad_request(error: &str) -> Self {
        Self {
            status: StatusCode::BAD_REQUEST,
            error: error.to_string(),
        }
    }

    pub fn missing_parameter(name: &str) -> Self {
        Self::bad_request(&format!("Parameter '{}' is misssing!", name))
    }

    pub fn into_error(self) -> actix_web::Error {
        actix_web::Error::from(self)
    }
}

impl Display for HttpError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}: {}", self.status, self.error)
    }
}

impl ResponseError for HttpError {
    fn status_code(&self) -> StatusCode {
        self.status
    }
}

impl From<Box<dyn Error>> for HttpError {
    fn from(e: Box<dyn Error>) -> Self {
        HttpError::server_error(&e.to_string())
    }
}

impl From<ExecError> for HttpError {
    fn from(e: ExecError) -> Self {
        HttpError::server_error(&e.get_message())
    }
}
