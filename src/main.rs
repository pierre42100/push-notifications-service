use std::path::Path;
use std::process;

use push_notification_service::config::conf;
use push_notification_service::utils::error::{new_err, Res};
use push_notification_service::{config, server};

fn init_config(conf_path: &Path) {
    if conf_path.exists() {
        eprintln!("Failed to initialize config : specified file already exists!");
        process::exit(-2);
    }

    if let Err(e) = std::fs::write(conf_path, include_bytes!("../config.sample.yaml")) {
        eprintln!("Failed to write configuration! {}", e);
        process::exit(-3);
    }

    println!(
        "Done writing configuration. You can not adapt the configuration to match your needs."
    );
}

async fn serve(conf_path: &Path) -> Res {
    config::Config::load(conf_path)?;

    if !conf().storage_path().is_dir() {
        return new_err("Storage path is not a directory!");
    }

    server::listener::start_server().await?;

    Ok(())
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let args = std::env::args().collect::<Vec<String>>();

    if args.len() != 3 {
        eprintln!("Usage: {} conf-file {{init-config|serve}}", args[0]);
        process::exit(-1);
    }

    let conf_file = Path::new(&args[1]);
    let action = args[2].as_str();

    match action {
        "init-config" => init_config(conf_file),

        "serve" => {
            if let Err(e) = serve(conf_file).await {
                eprintln!("Failed to start server! {}", e.get_message());
                std::process::exit(-4);
            }
        }

        _ => eprintln!("Unknown action!"),
    }

    Ok(())
}
