//! # Service configuration
//!
//! @author Pierre Hubert
//! # Project configuration

use std::path::Path;

use yaml_rust::YamlLoader;

use crate::utils::error::{new_err, Res};

pub struct Config {
    storage_path: String,
    pub listen_address: String,
    pub access_token: String,
}

static mut CONF: Option<Config> = None;

impl Config {
    pub fn load(conf_file: &Path) -> Res {
        if !conf_file.is_file() {
            return new_err("Configuration file does not exists or is not a file!");
        }

        // Parse YAML file
        let content = std::fs::read_to_string(conf_file)?;
        let docs = YamlLoader::load_from_str(&content)?;

        if docs.len() != 1 {
            return new_err("YAML file must contains strictly ONE document!");
        }

        let doc = &docs[0];

        let conf = Config {
            storage_path: doc["storage_path"]
                .as_str()
                .ok_or("storage_path missing!")?
                .to_string(),
            listen_address: doc["listen_address"]
                .as_str()
                .ok_or("listen_address missing!")?
                .to_string(),
            access_token: doc["access_token"]
                .as_str()
                .ok_or("access_token missing!")?
                .to_string(),
        };

        unsafe {
            CONF = Some(conf);
        }

        Ok(())
    }

    pub fn storage_path(&self) -> &Path {
        Path::new(&self.storage_path)
    }
}

pub fn conf() -> &'static Config {
    unsafe { CONF.as_ref().expect("Configuration not ready yet!") }
}
