//! # Random utilities

use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

/// Generate a random string of a given size
///
/// ```
/// use push_notification_service::utils::random::rand_str;
///
/// let size = 10;
/// let rand = rand_str(size);
/// assert_eq!(size, rand.len());
/// ```
pub fn rand_str(len: usize) -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .map(char::from)
        .take(len)
        .collect()
}
