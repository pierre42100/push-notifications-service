//! # Date utilities
//!
//! @author Pierre Hubert

use std::time::{SystemTime, UNIX_EPOCH};

/// Get the current time since epoch
///
/// ```
/// use push_notification_service::utils::date::time;
///
/// let time = time();
/// ```
pub fn time() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs()
}
