//! # Push notification service utilities
//!
//! @author Pierre Hubert

use std::fmt::Display;

use crate::data::http_error::HttpError;

pub type Res<E = ()> = Result<E, ExecError>;

#[derive(Debug, Clone)]
pub struct ExecError(String);

impl ExecError {
    pub fn new(msg: &str) -> Self {
        Self(msg.to_string())
    }

    pub fn get_message(&self) -> String {
        self.0.clone()
    }
}

pub fn new_err(msg: &str) -> Res {
    Err(ExecError(msg.to_string()))
}

impl<T: Display> From<T> for ExecError {
    fn from(e: T) -> Self {
        ExecError(format!("Error: {}", e))
    }
}

impl From<ExecError> for actix_web::Error {
    fn from(e: ExecError) -> Self {
        HttpError::server_error(&e.0).into_error()
    }
}
