//! # Project constants
//!
//! @author Pierre Hubert

use std::time::Duration;

/// Device tokens size
pub const TOKENS_SIZE: usize = 50;

/// Clean up interval (1 day)
pub const CLEANUP_INTERVAL: Duration = Duration::from_secs(3600 * 24);

/// Default notifications timeout (3 weeks)
pub const DEFAULT_TIMEOUT: Duration = Duration::from_secs(3600 * 24 * 7 * 3);
