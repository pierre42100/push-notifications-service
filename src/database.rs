//! # Database
//!
//! @author Pierre Hubert

use std::path::PathBuf;

use crate::config::conf;
use crate::constants;
use crate::constants::DEFAULT_TIMEOUT;
use crate::data::client_token::ClientToken;
use crate::data::notification::{NotifID, NotifList, Notification};
use crate::utils::date::time;
use crate::utils::error::Res;
use crate::utils::random::rand_str;

/// Get the file that stores the notifications of a device
fn client_file(s: &ClientToken) -> PathBuf {
    conf().storage_path().join(s.id())
}

/// Check out whether a token exists or not
pub fn does_token_exists(s: &ClientToken) -> bool {
    client_file(s).is_file()
}

pub fn get_clients_list() -> Res<Vec<ClientToken>> {
    Ok(std::fs::read_dir(conf().storage_path())?
        .into_iter()
        .map(|f| f.map(|f| f.file_name().to_string_lossy().to_string()))
        .map(|f| f.map(|f| ClientToken::new(&f)))
        .collect::<Result<Vec<ClientToken>, std::io::Error>>()?)
}

/// Get the list of notifications of a client
fn get_list(s: &ClientToken) -> Res<NotifList> {
    let str = std::fs::read_to_string(client_file(s))?;
    let list = serde_json::from_str(&str)?;
    Ok(list)
}

/// Take, remove and return all the notifications of a client
pub fn take_all_notifications(s: &ClientToken) -> Res<NotifList> {
    let list = get_list(s)?;
    save_list(s, vec![])?;
    Ok(list)
}

/// Save the list of notifications for a client
fn save_list(s: &ClientToken, list: NotifList) -> Res {
    std::fs::write(client_file(s), serde_json::to_string(&list)?)?;
    Ok(())
}

/// Create a new client
pub fn create_new_client() -> Res<ClientToken> {
    let token = ClientToken::new(&rand_str(constants::TOKENS_SIZE));
    save_list(&token, vec![])?;
    Ok(token)
}

/// Remove a client
pub fn remove_client(c: &ClientToken) -> Res {
    Ok(std::fs::remove_file(client_file(c))?)
}

/// Save a notification in the list of notification of a client
pub fn save_notification(c: &ClientToken, n: Notification) -> Res {
    let mut curr_list = get_list(c)?;
    curr_list.push(n);
    save_list(c, curr_list)
}

/// Remove a notification from the current list of notifications
pub fn remove_notification(c: &ClientToken, id: &NotifID) -> Res {
    let curr_list = get_list(c)?;
    save_list(c, curr_list.into_iter().filter(|p| !p.id.eq(id)).collect())
}

/// Replace a notification by another one. If the notification is not found, add it to the list
pub fn replace_notification(c: &ClientToken, n: Notification) -> Res {
    let curr_list = get_list(c)?;
    let mut new_list = curr_list
        .into_iter()
        .filter(|p| !p.id.eq(&n.id))
        .collect::<NotifList>();

    new_list.push(n);
    save_list(c, new_list)
}

/// Clean the list of notifications
pub fn clean_list() -> Res {
    for client in get_clients_list()? {
        let new_list = get_list(&client)?
            .into_iter()
            .filter(|n| n.time_create + n.timeout.unwrap_or(DEFAULT_TIMEOUT.as_secs()) > time())
            .collect();

        save_list(&client, new_list)?;
    }

    Ok(())
}
