//! # Server part (WebSocket)

use std::collections::HashMap;

use actix::prelude::*;
use actix::{Actor, Addr, Context};

use crate::api_data::PushNotificationResult;
use crate::constants::CLEANUP_INTERVAL;
use crate::data::client_token::ClientToken;
use crate::data::notification::{NotifID, Notification};
use crate::database;
use crate::server::client::{CloseConnection, DropNotification, SendNotification, WsClient};
use crate::utils::error::Res;

#[derive(Message)]
#[rtype(result = "()")]
pub struct NewConnection {
    pub client: ClientToken,
    pub addr: Addr<WsClient>,
}

/// Session is disconnected
#[derive(Message)]
#[rtype(result = "()")]
pub struct Disconnect {
    pub client: ClientToken,
    pub addr: Addr<WsClient>,
}

#[derive(Message)]
#[rtype(result = "Res")]
pub struct RemoveClient {
    pub client: ClientToken,
}

#[derive(Message)]
#[rtype(result = "Res<PushNotificationResult>")]
pub struct PushNotification {
    pub notification: Notification,
    pub clients: Vec<ClientToken>,
}

#[derive(Message)]
#[rtype(result = "Res")]
pub struct RemoveNotification {
    pub id: NotifID,
    pub clients: Option<Vec<ClientToken>>,
}

#[derive(Default)]
pub struct WsServer {
    sessions: HashMap<ClientToken, Addr<WsClient>>,
}

impl WsServer {
    fn drop_notification(&self, id: &NotifID, client: &ClientToken) {
        if let Some(conn) = self.sessions.get(client) {
            conn.do_send(DropNotification { id: id.clone() })
        }

        if let Err(e) = database::remove_notification(client, id) {
            eprintln!("Failed to remove notification: {}", e.get_message());
        }
    }
}

impl Actor for WsServer {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        ctx.run_interval(CLEANUP_INTERVAL, |_, _| {
            if let Err(e) = database::clean_list() {
                eprintln!("Failed to clean up notifications list: {}", e.get_message());
            } else {
                println!("Successfully cleaned up list!");
            }
        });
    }
}

impl Handler<NewConnection> for WsServer {
    type Result = ();

    fn handle(&mut self, msg: NewConnection, _: &mut Context<Self>) -> Self::Result {
        if let Some(old_conn) = self.sessions.insert(msg.client.clone(), msg.addr.clone()) {
            old_conn.do_send(CloseConnection {})
        }

        // Send all pending notifications to the client
        let notifs = match database::take_all_notifications(&msg.client) {
            Ok(n) => n,
            Err(e) => {
                eprintln!("Failed to take notifications! {}", e.get_message());
                return;
            }
        };

        for n in notifs {
            msg.addr.do_send(SendNotification { n });
        }
    }
}

impl Handler<Disconnect> for WsServer {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) -> Self::Result {
        if let Some(c) = self.sessions.get(&msg.client) {
            if c == &msg.addr {
                self.sessions.remove(&msg.client);
            }
        }
    }
}

impl Handler<RemoveClient> for WsServer {
    type Result = Res;

    fn handle(&mut self, msg: RemoveClient, _: &mut Context<Self>) -> Self::Result {
        database::remove_client(&msg.client)?;

        if let Some(c) = self.sessions.get(&msg.client) {
            c.do_send(CloseConnection {});
        }

        Ok(())
    }
}

impl Handler<PushNotification> for WsServer {
    type Result = Res<PushNotificationResult>;

    fn handle(&mut self, msg: PushNotification, _: &mut Context<Self>) -> Self::Result {
        let mut ok = vec![];
        let mut fail = vec![];

        for client in &msg.clients {
            // Send the notification directly
            if let Some(conn) = self.sessions.get(client) {
                conn.do_send(SendNotification {
                    n: msg.notification.clone(),
                });
                ok.push(client.clone());
                continue;
            }

            // Save the notification
            if let Err(e) = database::replace_notification(client, msg.notification.clone()) {
                eprintln!("Failed to push notification: {}", e.get_message());
                fail.push(client.clone());
            } else {
                ok.push(client.clone());
            }
        }

        Ok(PushNotificationResult { ok, fail })
    }
}

impl Handler<RemoveNotification> for WsServer {
    type Result = Res;

    fn handle(&mut self, msg: RemoveNotification, _: &mut Self::Context) -> Self::Result {
        // Check if specific clients where specified in the request
        if let Some(clients) = msg.clients.as_ref() {
            for client in clients {
                self.drop_notification(&msg.id, client);
            }
        } else {
            for client in database::get_clients_list()? {
                self.drop_notification(&msg.id, &client);
            }
        }

        Ok(())
    }
}
