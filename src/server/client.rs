//! # Client part (WebSocket)

use std::time::{Duration, Instant};

use actix::prelude::*;
use actix::{Actor, ActorContext, Addr, AsyncContext, Running, StreamHandler};
use actix_web_actors::ws;
use actix_web_actors::ws::{Message, ProtocolError};
use serde::Serialize;

use crate::data::client_token::ClientToken;
use crate::data::notification::{NotifID, Notification};
use crate::server::server_actor;
use crate::server::server_actor::{NewConnection, WsServer};

/// How often heartbeat pings are sent
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(30);

/// How long before lack of client response causes a timeout
const CLIENT_TIMEOUT: Duration = Duration::from_secs(60);

/// Session is disconnected
#[derive(Message)]
#[rtype(result = "()")]
pub struct CloseConnection {}

#[derive(Message)]
#[rtype(result = "()")]
pub struct SendNotification {
    pub n: Notification,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct DropNotification {
    pub id: NotifID,
}

#[derive(Serialize)]
struct DropNotificationMessage {
    drop_id: String,
}

pub struct WsClient {
    pub client: ClientToken,
    pub srv: Addr<WsServer>,
    pub hb: Instant,
}

impl Actor for WsClient {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);

        self.srv.do_send(NewConnection {
            client: self.client.clone(),
            addr: ctx.address(),
        })
    }

    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        self.srv.do_send(server_actor::Disconnect {
            client: self.client.clone(),
            addr: ctx.address(),
        });

        Running::Stop
    }
}

impl WsClient {
    /// helper method that sends ping to client every second.
    ///
    /// also this method checks heartbeats from client
    fn hb(&self, ctx: &mut ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            // check client heartbeats
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                // heartbeat timed out
                println!("Websocket Client heartbeat failed, disconnecting!");

                // stop actor
                ctx.stop();

                // don't try to send a ping
                return;
            }

            ctx.ping(b"");
        });
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsClient {
    fn handle(&mut self, msg: Result<Message, ProtocolError>, ctx: &mut Self::Context) {
        let msg = match msg {
            Err(e) => {
                eprintln!("WS error: {:?}", e);
                ctx.stop();
                return;
            }
            Ok(msg) => msg,
        };

        match msg {
            Message::Text(_) => {}
            Message::Binary(_) => {}
            Message::Continuation(_) => {}
            Message::Ping(d) => {
                self.hb = Instant::now();
                ctx.pong(&d);
            }
            Message::Pong(_) => {
                self.hb = Instant::now();
            }
            Message::Close(_) => {
                ctx.stop();
            }
            Message::Nop => {}
        }
    }
}

impl Handler<CloseConnection> for WsClient {
    type Result = ();

    fn handle(&mut self, _: CloseConnection, ctx: &mut Self::Context) -> Self::Result {
        ctx.close(None);
    }
}

impl Handler<SendNotification> for WsClient {
    type Result = ();

    fn handle(&mut self, n: SendNotification, ctx: &mut Self::Context) -> Self::Result {
        let to_send = match serde_json::to_string(&n.n) {
            Ok(str) => str,
            Err(e) => {
                eprintln!("Failed to serialize notification! {}", e);
                return;
            }
        };

        ctx.text(to_send)
    }
}

impl Handler<DropNotification> for WsClient {
    type Result = ();

    fn handle(&mut self, n: DropNotification, ctx: &mut Self::Context) -> Self::Result {
        let to_send = match serde_json::to_string(&DropNotificationMessage { drop_id: n.id }) {
            Ok(str) => str,
            Err(e) => {
                eprintln!("Failed to serialize message! {}", e);
                return;
            }
        };

        ctx.text(to_send)
    }
}
