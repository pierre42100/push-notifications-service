//! # Push notification server
//!
//! @author Pierre Hubert

use std::time::Instant;

use actix::{Actor, Addr};
use actix_web::web::Data;
use actix_web::{web, App, HttpRequest, HttpResponse, HttpServer};
use serde::{Deserialize, Serialize};

use crate::api_data::{ClientCreated, ClientDestroyed, RemoveNotificationResult};
use crate::config::conf;
use crate::data::client_token::ClientToken;
use crate::data::http_error::HttpError;
use crate::data::notification::Notification;
use crate::database;
use crate::server::client::WsClient;
use crate::server::server_actor::{PushNotification, RemoveClient, RemoveNotification, WsServer};
use crate::utils::date::time;

#[derive(Serialize, Deserialize)]
pub struct PostParams {
    client: Option<ClientToken>,

    clients: Option<Vec<ClientToken>>,
    id: Option<String>,
    timeout: Option<u64>,
    title: Option<String>,
    body: Option<String>,
    image: Option<String>,
}

async fn check_auth(req: &HttpRequest) -> actix_web::Result<()> {
    let token = req
        .headers()
        .get("token")
        .map(|t| String::from_utf8_lossy(t.as_bytes()).to_string());

    if let Some(token) = token {
        if token.eq(&conf().access_token) {
            return Ok(());
        }
    }

    Err(HttpError::unauthorized("Missing or invalid token!").into_error())
}

/// Create a new device token
async fn create_token(req: HttpRequest) -> actix_web::Result<HttpResponse> {
    check_auth(&req).await?;

    let client = database::create_new_client()?;

    Ok(HttpResponse::Ok().json(ClientCreated { token: client }))
}

/// Destroy a token
async fn remove_token(
    req: HttpRequest,
    params: web::Json<PostParams>,
    srv: web::Data<Addr<WsServer>>,
) -> actix_web::Result<HttpResponse> {
    check_auth(&req).await?;

    let client = match &params.client {
        None => return Err(HttpError::missing_parameter("client").into_error()),
        Some(client) => client,
    };

    match srv
        .send(RemoveClient {
            client: client.clone(),
        })
        .await
    {
        Ok(res) => {
            res?;
            Ok(HttpResponse::Ok().json(ClientDestroyed {}))
        }
        Err(e) => {
            log::error!("Failed to remove a token! {:#?}", e);
            Ok(HttpResponse::InternalServerError().json("Failed to remove token"))
        }
    }
}

/// Push a notification
async fn push_notification(
    req: HttpRequest,
    params: web::Json<PostParams>,
    srv: web::Data<Addr<WsServer>>,
) -> actix_web::Result<HttpResponse> {
    check_auth(&req).await?;

    let id = match &params.id {
        None => return Err(HttpError::missing_parameter("id").into_error()),
        Some(id) => id.to_string(),
    };

    let clients = match &params.clients {
        None => return Err(HttpError::missing_parameter("clients").into_error()),
        Some(id) => id,
    };

    let notification = Notification {
        id,
        time_create: time(),
        timeout: params.timeout,
        title: params.title.clone(),
        body: params.body.clone(),
        image: params.image.clone(),
    };

    let result = srv
        .send(PushNotification {
            clients: clients.clone(),
            notification,
        })
        .await;

    match result {
        Ok(r) => Ok(HttpResponse::Ok().json(r?)),
        Err(e) => {
            log::error!("Failed to push a notification! {:?}", e);
            Ok(HttpResponse::InternalServerError().finish())
        }
    }
}

/// Remove an old notification
async fn remove_notification(
    req: HttpRequest,
    params: web::Json<PostParams>,
    srv: web::Data<Addr<WsServer>>,
) -> actix_web::Result<HttpResponse> {
    check_auth(&req).await?;

    let id = match &params.id {
        None => return Err(HttpError::missing_parameter("id").into_error()),
        Some(id) => id,
    };

    let result = srv
        .send(RemoveNotification {
            clients: params.clients.clone(),
            id: id.clone(),
        })
        .await;

    match result {
        Ok(res) => {
            res?;
            Ok(HttpResponse::Ok().json(RemoveNotificationResult {}))
        }
        Err(e) => {
            log::error!("Failed to remove a notification! {:?}", e);
            Ok(HttpResponse::InternalServerError().finish())
        }
    }
}

/// WebSocket route
async fn ws_route(
    req: HttpRequest,
    stream: web::Payload,
    srv: web::Data<Addr<WsServer>>,
) -> actix_web::Result<HttpResponse> {
    // Get client token
    let token = ClientToken::new(req.uri().to_string().split('/').last().unwrap_or("BAD"));

    if !database::does_token_exists(&token) {
        return Err(HttpError::unauthorized("Token not recognized!").into_error());
    }

    actix_web_actors::ws::start(
        WsClient {
            hb: Instant::now(),
            client: token,
            srv: srv.get_ref().clone(),
        },
        &req,
        stream,
    )
}

/// Start the main server
pub async fn start_server() -> std::io::Result<()> {
    let bind = conf().listen_address.clone();
    println!("Starting server at: {}", bind);

    let server = WsServer::default().start();

    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(server.clone()))
            // WebSocket route
            .service(web::resource("/ws/{token}").to(ws_route))
            // Notifications push
            .service(web::resource("/create_client").to(create_token))
            .service(web::resource("/remove_client").to(remove_token))
            .service(web::resource("/push_notification").to(push_notification))
            .service(web::resource("/remove_notification").to(remove_notification))
    })
    .bind(&bind)?
    .run()
    .await
}
