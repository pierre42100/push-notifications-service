//! # API data
//!
//! @author Pierre Hubert

use serde::Serialize;

use crate::data::client_token::ClientToken;

#[derive(Serialize)]
pub struct ClientCreated {
    pub token: ClientToken,
}

#[derive(Serialize)]
pub struct ClientDestroyed {}

#[derive(Serialize)]
pub struct PushNotificationResult {
    pub ok: Vec<ClientToken>,
    pub fail: Vec<ClientToken>,
}

#[derive(Serialize)]
pub struct RemoveNotificationResult {}
